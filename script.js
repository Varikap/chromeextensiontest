async function fetchData() {
  console.log('test')
  const url = 'https://catfact.ninja/fact'
  const options = {
    method: 'GET'
  }

  try {
    const response = await fetch(url, options);
    const result = await response.json();
    document.getElementById('fact').innerHTML = result.fact
    console.log(result);
  } catch (error) {
    console.error(error);
  }
}
fetchData()

document.getElementById('again').addEventListener('click', fetchData)

